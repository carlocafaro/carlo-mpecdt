Computational Lab: Dynamics 5
=============================

This is the tutorial material for Computational Lab 5 in the Dynamics
core course of the MPECDT MRes.

As for all of the computational labs in this course, you should
develop your solutions to these exercises in a git repository, hosted
on BitBucket. During the timetabled laboratory: 

- Work through the exercises on your laptop, by forking and checking out
  a copy of the mpcdt git repository. 
- If you would like help with issues in your code, either:

  - Paste the code into a gist, or
  - Push your code up to your forked git repository on BitBucket.
    Then, share the link to your gist or repository in the IRC channel

.. container:: tasks

   After merging the latest version of the mpecdt repository (git
   merge mpecdt/master), you will find a file to the mpecdt/src
   directory called `bistable.py`, in which you should add all of the
   tasks from this tutorial.

   Exercises X and X will be assessed as part of the coursework for
   this project. Solutions to the exercises should execute as part of
   the script; use print statements to answer any questions in the
   exercises.  Please email `colin.cotter@imperial.ac.uk` the git
   revision for your repository in the form of a SHA (just type `git
   log` to get the list of SHAs for each revision) before midnight GMT
   on 12th December 2014. The marks will be allocated as follows: 50\%
   for Exercise 2, 50\% for Exercise 3.

This Computational Lab is about the Fokker-Planck equation and
invariant measures.

In this computer lab we will consider stochastic differential
equations (SDEs) of the form 

.. math::
   dx = -V'(x)dt + \sigma dW,
   :label: SDE 

where :math:`V(x)` is a prescribed potential function, :math:`V'(x)`
is its derivative, and :math:`\sigma>0` is a diffusion coefficient.
The dynamics arising from this equation is known as "Brownian
dynamics". The use of this model is inspired by Mitchell et al (2011).

If :math:`\sigma=0` then this becomes the ordinary differential
equation in the form of a "gradient flow",

.. math::
   \frac{dx}{dt} = -V'(x),

which has solutions that converge towards one of the minima of
:math:`V(x)`. When :math:`\sigma>0`, there are two competing terms,
the gradient flow and the noise, which means that the particle 
position :math:`x(t)` can move between the minima of V. 

In this lab we will concentrate on the case

.. math::
   V(x) = \frac{x^4}{4} - \frac{x^2}{2}.

This function has two minima at x=1 and x=-1.

Exercise 1
----------

.. container:: tasks

   The code in `bistable.py` implements the Euler-Maruyama method for
   the Brownian dynamics model, with this potential. Execute the code
   to visualise the solution, which undergoes random transitions
   between the positive and negative "wells" in the
   potential. Experiment with changing :math:`\sigma` to see how it
   affects the dynamics (you may need to change the time interval over
   which the equations are solved to see what is going on).

Exercise 2
----------

To understand the dynamics of this model, we consider a random
variable :math:`X(t)`, which has some chosen probability distribution
with PDF :math:`\rho(x,0)` at time t=0. At later times, the PDF
evolves according to the Fokker-Planck equation, which for this
equation takes the form

.. math::
   \frac{\partial\rho}{\partial t} = \frac{\partial}{\partial x}\left(
   V'(x)\rho\right) + \frac{\sigma^2}{2}\frac{\partial^2\rho}{\partial x^2}.

This equation looks like a one-dimensional advection-diffusion equation
for :math:`\rho`, and it can be shown that all solutions converge to a 
steady state, i.e. an invariant measure, which satisfies

.. math:: 
   0 = \frac{\partial}{\partial x}\left( V'(x)\hat{\rho}\right) +
   \frac{\sigma^2}{2}\frac{\partial^2\hat{\rho}}{\partial x^2}.

Since this equation is an elliptic PDE, it has a unique solution, given by

.. math::
   \hat{\rho(x)} = Ce^{-\frac{2}{\sigma^2}V(x)}.

We can solve this PDE approximately, using a Monte Carlo method (also
known as an "ensemble" method in the weather prediction community). To
do this we repeatedly draw samples :math:`X_i(0)` from the initial
condition PDF :math:`\rho(x,0)`. We then solve Equation :eq:`SDE` (or
a numerical approximation of it) using :math:`X_i(0)` as an initial
condition, to obtain samples :math:`X_i(t)` from the distribution with
PDF :math:`\rho(x,t)`. These can then be used to compute statistics
from the PDF, using the Monte-Carlo approximation

.. math::

   \int f(x)\rho(x)dx = \mathbb{E}\left[f(X(t))\right] \approx
   \frac{1}{N}\sum_{i=1}^N f(X_i).

.. container:: tasks

   Choosing the initial condition :math:`X(0)=1` with probability 1,
   implement an ensemble method for solving the Fokker-Planck equation
   above. Visualise the solution at various times using a histogram
   plot, which is provided by 'pylab.hist'. On the same axes, plot the
   invariant solution above for comparison. How quickly does the
   solution converge to the steady state solution? (Make a qualitative
   answer.)

Exercise 3
----------

   Since Equation :eq:`SDE` has a unique invariant measure which all
   solutions of the Fokker-Planck equation converge to as
   :math:`t\to\infty`, we know that Equation :eq:`SDE` is ergodic. This
   means that if we take a single solution :math:`X(t_n)` at discrete
   time intervals :math:`t_n=n\delta t`, then 

   .. math::
      \lim_{N\to\infty} \frac{1}{N}\sum_{n=1}^N f(X(t_n))
      = \mathbb{E}\left[f(X)\right].

   .. container:: tasks
	       
      For the functions :math:`f(x)=x` and :math:`f(x)=x^2`, plot

      .. math::
	 \frac{1}{N}\sum_{n=1}^N f(X(t_n))

      against N, and demonstrate that this value is converging as N gets
      larger. What is the rate of convergence (i.e. polynomial,
      exponential etc.)? Explain your answer with a suitable plot.


References
----------

Mitchell, Lewis, and Georg A. Gottwald. "Data assimilation in
slow-fast systems using homogenized climate models." arXiv preprint
arXiv:1110.6671 (2011).

