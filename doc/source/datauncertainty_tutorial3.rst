Computational Lab: Data and Uncertainty 3
=============================

This is the tutorial material for Computational Lab 3 in the Data and
Uncertainty core course of the MPECDT MRes.

As for all of the computational labs in this course, you should
develop your solutions to these exercises in a git repository, hosted
on BitBucket. During the timetabled laboratory: 

- Work through the exercises on your laptop, by forking and checking out
  a copy of the mpecdt git repository. 
- If you would like help with issues in your code, either:

  - Paste the code into a gist, or
  - Push your code up to your forked git repository on BitBucket.
    Then, share the link to your gist or repository in the IRC channel

.. container:: tasks  

   After merging the latest version of the mpecdt repository, you will
   find a file to the mpecdt/src directory called `importance.py`, in
   which you should add all of the tasks from this tutorial.
   Exercises 1 and 2 will be assessed as part of the coursework for
   this project. Solutions to the exercises should execute as part of
   the script; use print statements to answer any questions in the
   exercises.  Please email `colin.cotter@imperial.ac.uk` the git
   revision for your repository in the form of a SHA (just type `git
   log` to get the list of SHAs for each revision), before the
   submission date (28th November 2014). The marks will be allocated
   as follows: 70\% for Exercise 1, 30\% for Exercise 2, Exercises
   will be graded for correct implementation and good Python coding
   style (use of functions, code reuse, efficiency, readability, use
   of Numpy functions etc.).

This Computational Lab is about how to use Monte Carlo methods when we
don't have a random number generator that can produce samples from the
random variable :math:`X`. One motivation for this is in data
assimilation, where we obtain very complicated probability
distributions for the state variables having observed some data.

We continue to be interested in computing statistics of the form:
:math:`\mathbb{E}[f(X)]`, which satisfies

.. math::
   \theta := \mathbb{E}[f(X)] = \int f(x) \mu(dx),

where :math:`\mu` is the probability measure for :math:`X`.

Say that there exists a probability density function (PDF) such that
we can write :math:`\mu(dx) = \rho(x)dx`. On a computer, it is
relatively easy to simulate uniform random variables using modulo
arithmetic, and there exist some standard variable transformations to
get from uniform random variables to Normal, exponential, gamma
distributions etc. What if we have something else?

One trick is to rewrite 

.. math::
   \int f(x) \rho(x)dx = \int f(x)\frac{\rho(x)}{\rho'(x)}\rho'(x)dx,

where :math:`\rho'(x)` is the PDF for a random variable that we can
simulate directly. A Monte Carlo estimator for this is

.. math::
   \int f(x) \rho(x)dx \approx \frac{1}{N}
   \sum_{i=1}^Nf(Y_i)
   \frac{\rho(Y_i)}{\rho'(Y_i)},

where :math:`\{Y_i\}_{i=1}^N` are independent samples for the random
variable :math:`Y` with PDF :math:`\rho'(x)`.

This estimator is biased, in particular it computes the wrong value
for the case :math:`f(x)=1`.

.. container:: tasks  
   Verify this.

We can fix this by correcting the factor :math:`1/N` so that it
computes the correct value for :math:`f(x)=1`,

.. math::
   \int f(x) \rho(x)dx \approx \frac{1}
   {\sum_{i=1}^N\frac{\rho(Y_i)}{\rho'(Y_i)}}
   \sum_{i=1}^Nf(Y_i)
   \frac{\rho(Y_i)}{\rho'(Y_i)},

This technique is called "importance sampling". It can be shown that
importance sampling produces convergent results as :math:`N\to\infty`,
provided that 

.. math::
   \rho(x)=0 \mbox{ if } \rho'(x)=0.

(Technical remark: this implies that the measure :math:`\rho(x)dx` is
absolutely continuous with respect to the measure :math:`\rho'(x)dx`.)

In other words, the importance sampling estimate of :math:`\mathbb{E}[f(X)]`
is given by

.. math::
   \mathbb{E}[f(X)] \approx \sum_{i=1}^M w_i f(Y_i'),

where :math:`\{Y_i\}` are N independent samples from the random variable
:math:`Y`. The weights :math:`\{w_i\}` are given by

.. math::
   w_i \propto \frac{\rho(Y_i)}{\rho'(Y_i)},

where the constant of proportionality is chosen such that
:math:`\sum_i w_i = 1`.
An important detail is that since :math:`\rho` and :math:`\rho'` only
appear in the form of a ratio, they can both be scaled by arbitrary
constants. This means that we can neglect tedious normalisation constants
such as the factor of :math:`1/\sqrt{2\pi\sigma^2}` in the PDF for the
Normal distribution.

Exercise 1
The file `mpecdt/src/importance_rejection.py` contains an incomplete
implementation of the importance sampling technique. ::

  def square(x):
     """
     Function to compute the square of the input. If the input is 
     a numpy array, this returns a numpy array of all of the squared values.
     inputs:
     x - a numpy array of values

     outputs:
     y - a numpy array containing the square of all of the values
     """
     return x**2

  def normal(N):
     """
     Function to return a numpy array containing N samples from 
     a N(0,1) distribution.
     """
     return np.random.randn(N)

  def normal_pdf(x):
     """
     Function to evaluate the PDF for the normal distribution (the
     normalisation coefficient is ignored). If the input is a numpy
     array, this returns a numpy array of all of the squared values.

     inputs:
     x - a numpy array of input values
  
     outputs:
     f - a numpy array of f(x)
     """
     return exp(-x**2/2)

  def uniform_pdf(x):
     """
     Function to evaluate the PDF for the standard uniform
     distribution. If the input is a numpy array, this returns a numpy
     array of all of the squared values.

     inputs:
     x - a numpy array of input values
  
     outputs:
     f - a numpy array of f(x)
     """
     y = np.ones(x.shape)
     y[y<0] = 0.0
     y[y>1] = 0.0
     return y

  def importance(f, Y, rho, rhoprime, N):
     """
     Function to compute the Monte Carlo estimate of the expectation
     E[f(X)], with N samples

     inputs:
     f - a Python function that evaluates a chosen mathematical function on
     each entry in a numpy array
     Y - a Python function that takes N as input and returns
     independent individually distributed random samples from a chosen
     probability distribution
     rho - a Python function that evaluates the PDF for the desired distribution
     on each entry in a numpy array
     rhoprime - a Python function that evaluates the PDF for the
     distribution for Y, on each entry in a numpy array
     N - the number of samples to use
     """
  
     theta = FINISH THIS CODE!

     return theta

.. container:: tasks

   Complete the importance sampling code and verify it by using it to
   estimate the mean of the the uniform distribution using samples
   from the N(0,1) distribution. Demonstrate this by showing a log-log
   plot of the error of the estimate as a function of :math:`N`.

Exercise 2
----------

In this example, we illustrate importance sampling as a method
for reducing the variance of a Monte Carlo estimator.

We consider the approximation of the expectation value

.. math::

   \mathbb{E}[e^{-10X}\cos(X)] = \int_0^1 e^{-10 x} \cos (x) {\rm d}x ,

with the random variable  `X` having a uniform distribution on [0,1].

The analytic value of this expectation is given by

.. math::

   \mathbb{E}[e^{-10X}\cos(X)] = \frac{10}{101} - \frac{10 \cos(1) - \sin(1)}{101 e^{10}}.

One approach is to approximate this value by standard Monte Carlo,

.. math::

   \bar x_M = \frac{1}{M} \sum_{i=1}^M \cos(u_i) e^{-10u_i},

where :math:`u_i` are samples from the uniform distribution.

Since :math:`e^{-10x}` decays very rapidly away from zero, the
standard Monte Carlo is very slow to converge. It might converge
faster if we sample from another distribution which is more
concentrated around zero, and then apply importance sampling (sampling
from different distributions to speed up convergence is a common theme
in computational statistics).

For example, we may take :math:`Y` to have
PDF

.. math::
   \pi_{Y}(x) = \left\{ \begin{array}{ll}  \frac{10 e^{-10x}}{1 - e^{-10}} &
   \mbox{if} \,\,x\in [0,1], \\ 0 & \mbox{otherwise.} \end{array} \right.

Samples from this distribution can be generated using the explicit formula

.. math::

   x_i = -0.1 \log(1-u_i + u_i e^{-10} )

where :math:`u_i` are uniform random variables on the interval [0,1].

.. container:: tasks

   Add a new function to the file that samples from this clustered
   distribution (by sampling from a uniform distribution and applying
   the transformation above), copying the interface for the `normal`
   function that was provided.

   Evaluate the expectation using importance sampling. Demonstrate that
   it works by plotting the error on a log-log graph.


